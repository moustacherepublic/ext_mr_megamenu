<?php
/**
 * MR_Megamenu extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       MR
 * @package        MR_Megamenu
 * @copyright      Copyright (c) 2014
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Megamenu setup
 *
 * @category    MR
 * @package     MR_Megamenu
 * @author      Ultimate Module Creator
 */
class MR_Megamenu_Model_Resource_Setup
    extends Mage_Catalog_Model_Resource_Setup {
}
