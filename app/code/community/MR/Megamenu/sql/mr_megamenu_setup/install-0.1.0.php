<?php
/**
 * MR_Megamenu extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       MR
 * @package        MR_Megamenu
 * @copyright      Copyright (c) 2014
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Megamenu module install script
 *
 * @category    MR
 * @package     MR_Megamenu
 * @author      Ultimate Module Creator
 */
$this->startSetup();

$this->addAttribute('catalog_category', 'megamenu_image1', array(
    'type'          => 'varchar',
    'label'         => 'Image 1',
    'input'         => 'image',
    'backend'       => 'catalog/category_attribute_backend_image',
    'required'      => false,
    'user_defined' => 1,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'group'         => 'Mega Menu'
));

$this->addAttribute('catalog_category', 'megamenu_link1', array(
    'group'         => 'Mega Menu',
    'input'         => 'text',
    'type'          => 'varchar',
    'label'         => 'Link 1',
    'backend'       => '',
    'visible'       => 1,
    'required'      => 0,
    'user_defined' => 1,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
));

$this->addAttribute('catalog_category', 'megamenu_image2', array(
    'type'          => 'varchar',
    'label'         => 'Image 2',
    'input'         => 'image',
    'backend'       => 'catalog/category_attribute_backend_image',
    'required'      => false,
    'user_defined' => 1,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'group'         => 'Mega Menu'
));

$this->addAttribute('catalog_category', 'megamenu_link2', array(
    'group'         => 'Mega Menu',
    'input'         => 'text',
    'type'          => 'varchar',
    'label'         => 'Link 2',
    'backend'       => '',
    'visible'       => 1,
    'required'      => 0,
    'user_defined' => 1,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
));

$this->addAttribute('catalog_category', 'megamenu_image3', array(
    'type'          => 'varchar',
    'label'         => 'Image 3',
    'input'         => 'image',
    'backend'       => 'catalog/category_attribute_backend_image',
    'required'      => false,
    'user_defined' => 1,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'group'         => 'Mega Menu'
));

$this->addAttribute('catalog_category', 'megamenu_link3', array(
    'group'         => 'Mega Menu',
    'input'         => 'text',
    'type'          => 'varchar',
    'label'         => 'Link 3',
    'backend'       => '',
    'visible'       => 1,
    'required'      => 0,
    'user_defined' => 1,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
));

$this->addAttribute('catalog_category', 'megamenu_image4', array(
    'type'          => 'varchar',
    'label'         => 'Image 4',
    'input'         => 'image',
    'backend'       => 'catalog/category_attribute_backend_image',
    'required'      => false,
    'user_defined' => 1,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'group'         => 'Mega Menu'
));

$this->addAttribute('catalog_category', 'megamenu_link4', array(
    'group'         => 'Mega Menu',
    'input'         => 'text',
    'type'          => 'varchar',
    'label'         => 'Link 4',
    'backend'       => '',
    'visible'       => 1,
    'required'      => 0,
    'user_defined' => 1,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
));

$this->addAttribute('catalog_category', 'megamenu_image5', array(
    'type'          => 'varchar',
    'label'         => 'Image 5',
    'input'         => 'image',
    'backend'       => 'catalog/category_attribute_backend_image',
    'required'      => false,
    'user_defined' => 1,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'group'         => 'Mega Menu'
));

$this->addAttribute('catalog_category', 'megamenu_link5', array(
    'group'         => 'Mega Menu',
    'input'         => 'text',
    'type'          => 'varchar',
    'label'         => 'Link 5',
    'backend'       => '',
    'visible'       => 1,
    'required'      => 0,
    'user_defined' => 1,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
));

$this->addAttribute('catalog_category', 'megamenu_columns', array(
    'group'         => 'Mega Menu',
    'input'         => 'text',
    'type'          => 'varchar',
    'label'         => 'Column Count',
    'backend'       => '',
    'visible'       => 1,
    'required'      => 0,
    'user_defined' => 1,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
));
$this->endSetup();
