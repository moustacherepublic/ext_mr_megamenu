<?php
/**
 * MR_Megamenu extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       MR
 * @package        MR_Megamenu
 * @copyright      Copyright (c) 2014
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Megamenu module upgrade script
 *
 * @category    MR
 * @package     MR_Megamenu
 * @author      Michael Zhang
 */
$this->startSetup();



$this->addAttribute('catalog_category', 'megamenu_custom_link', array(
    'group'         => 'Mega Menu',
    'input'         => 'text',
    'type'          => 'varchar',
    'label'         => 'Custom Link',
    'backend'       => '',
    'visible'       => 1,
    'required'      => 0,
    'user_defined' => 1,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'used_in_product_listing' => 1,
));

$this->endSetup();
