<?php
/**
 * MR_Megamenu extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category       MR
 * @package        MR_Megamenu
 * @copyright      Copyright (c) 2014
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
class MR_Megamenu_Block_Topmenu extends Mage_Page_Block_Html_Topmenu{
    const COLUMN_COUNT = 4;

    protected function _getHtml(Varien_Data_Tree_Node $menuTree, $childrenWrapClass)
    {
        $html = '';

        $children = $menuTree->getChildren();
        $parentLevel = $menuTree->getLevel();
        $childLevel = is_null($parentLevel) ? 0 : $parentLevel + 1;

        $counter = 1;
        $childrenCount = $children->count();

        $parentPositionClass = $menuTree->getPositionClass();
        $itemPositionClassPrefix = $parentPositionClass ? $parentPositionClass . '-' : 'nav-';

        $childrenWrapClass = 'nav-column';
        foreach ($children as $child) {
            $parts = explode('-', $child->getId());
            $categoryId = $parts[2];
            $childCategory = Mage::getModel('catalog/category')->load($categoryId);
//            $child->setLevel($childLevel);
//            $child->setIsFirst($counter == 1);
//            $child->setIsLast($counter == $childrenCount);
//            $child->setPositionClass($itemPositionClassPrefix . $counter);

            $outermostClassCode = '';
            $outermostClass = $menuTree->getOutermostClass();

            if ($childLevel == 0 && $outermostClass) {
                $outermostClassCode = ' class="' . $outermostClass . '" ';
                $child->setClass($outermostClass);
            }

            $html .= '<li>';
            $html .= '<a href="' . ($childCategory->getMegamenuCustomLink() ? : $child->getUrl()) . '" ' . $outermostClassCode . '>'
                . $this->escapeHtml($child->getName()) . '</a>';


            if ($childCategory->hasChildren()) {
                $html .= '<div>';
                $childrenCount = $child->getChildren()->count();
                $columnCount = $childCategory->getMegamenuColumns() ? : self::COLUMN_COUNT;
                $baseEachColumnCount = floor($childrenCount / $columnCount);
                $extraCount = $childrenCount % $columnCount;
                $html .= '<div class="' . $childrenWrapClass . '">';
                $html .= '<ul>';
                $counter = 0;
                $index = 0;
                foreach($childCategory->getChildrenCategories() as $cchild){
                    $html .= '<li>';
                    $html .= '<a href="' . ($cchild->getMegamenuCustomLink() ? : $cchild->getUrl()) . '" ' . $this->_getRenderedMenuItemAttributes($cchild) . '>' . $this->escapeHtml($cchild->getName()) . '</a>';
                    if($cchild->hasChildren() && Mage::helper('mr_megamenu')->getMaxDepth() == '3'){
                        $html .= '<ul class="level' . $childLevel . '">';
                        foreach($cchild->getChildrenCategories() as $ccchild){
                            $html .= '<li>';
                            $html .= '<a href="' . ($ccchild->getMegamenuCustomLink() ? : $ccchild->getUrl()) . '">' . $this->escapeHtml($ccchild->getName()) . '</a>';
                            $html .= '</li>';
                        }
                        $html .= '</ul>';
                    }
                    $html .= '</li>';
                    if(($index++ < ($childrenCount-1)) && (++$counter == ($baseEachColumnCount + ($extraCount>0 ? 1 : 0)))){
                        $html .= '</ul></div><div class="' . $childrenWrapClass . '"><ul>';
                        $counter = 0;
                        $extraCount--;
                    }

                }
                $html .= '</ul></div>';
                //images
                for($i=1; $i<=5; $i++){
                    $html .= $this->_getImageHtml($childCategory, $i);
                }

                $html .= '</div>';
            }
            $html .= '</li>';

        }
        return $html;
    }

    protected function _getImageHtml($category, $index){
        $html = '';
        $methodImage = "getMegamenuImage$index";
        $methodLink = "getMegamenuLink$index";
        $image = $category->$methodImage();
        $imageLink = $category->$methodLink();
        if($image){
            $imageSrc = Mage::getBaseUrl('media', true).'catalog/category/' . $image;
            if($imageLink){
                $html = '<a href="' . $imageLink . '"><img src="' . $imageSrc . '" class="image"/></a>';
            }
            else{
                $html = '<img src="' . $imageSrc . '" class="image"/>';
            }
        }

        return $html;
    }
}
