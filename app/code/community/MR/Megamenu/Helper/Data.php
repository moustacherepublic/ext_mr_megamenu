<?php
/**
 * MR_Megamenu extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       MR
 * @package        MR_Megamenu
 * @copyright      Copyright (c) 2014
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Megamenu default helper
 *
 * @category    MR
 * @package     MR_Megamenu
 * @author      Michael
 */
class MR_Megamenu_Helper_Data
    extends Mage_Core_Helper_Abstract {
    const XML_PATH_MEGAMENU_MAX_DEPTH = 'mr_megamenu/general/depth';

    /**
     * convert array to options
     * @access public
     * @param $options
     * @return array
     * @author Michael
     */
    public function convertOptions($options){
        $converted = array();
        foreach ($options as $option){
            if (isset($option['value']) && !is_array($option['value']) && isset($option['label']) && !is_array($option['label'])){
                $converted[$option['value']] = $option['label'];
            }
        }
        return $converted;
    }

    /**
     * get max depth of megamenu
     * @access public
     * @return string
     * @author Michael
     */
    public function getMaxDepth(){
        return  Mage::getStoreConfig(self::XML_PATH_MEGAMENU_MAX_DEPTH);
    }
}
