;(function($){
    $.fn.mrMegamenu = function(options){
        var defaults = {
//            columnWidth : '200px',
//            columnCount : '4',
            mode        : 'column'
        };

        var settings = $.extend({}, defaults, options);

        var $mrMegamenu = this;

        $mrMegamenu.initRowItems = function(){
            if(settings.mode == 'column'){
//                $(this).children('li').each(function(){
//                    var totalItems = $(this).children('div').children('.nav-column').length;
//
//                    //add columnCount columns
//                    $(this).children('div').each(function(){
//                        for(var i=1; i <= settings.columnCount; i++){
//                            $(this).append('<div class="col col' + i + '" style="width:' + settings.columnWidth + '"></div>');
//                        }
//                    });
//
//                    //move each nav column to related .col
//                    var itemPerColumn = Math.floor(totalItems/settings.columnCount);
//                    var leftCount = totalItems%settings.columnCount;
//
//                    var navColumns = $(this).children('div').children('.nav-column');
//                    var itemIndex = 0;
//                    for(var j=1; j<=settings.columnCount; j++){
//
//                        var k = 0;
//                        var parent = $(this).children('div').children('.col' + j);
//                        for(; k < itemPerColumn; k++){
//                            navColumns.eq(itemIndex++).appendTo(parent);
//                        }
//
//                        if(leftCount-- > 0){
//                            navColumns.eq(itemIndex++).appendTo(parent);
//                        }
//                    }
//
//                });

                //set all columns the same height
                $(this).children('li').each(function(){
                    var max_height = 0;

                    $(this).children().children('.col').each(function(){
                        if(max_height < $(this).height()){
                            max_height = $(this).height();
                        }
                    });
                    $(this).children().children('.col').each(function(){
                        $(this).height(max_height);
                    });

                    $(this).children('div').children('.col').each(function(){
                        if($(this).html() == '' || $(this).attr('class').substr($(this).attr('class').length-1) == settings.columnCount){
                            $(this).addClass('last');
                        }
                    });
                });


            }
            else{
                //set column width
//                $(this).children('li').each(function(){
//                    var i = 1;
//                    $(this).children().children('.nav-column').each(function(){
//                        $(this).css('width', settings.columnWidth);
//                        if(i++ % settings.columnCount == 1){
//                            $(this).addClass('first');
//                        }
//                    });
//                });

                //set all columns the same height
                $(this).children('li').each(function(){
                    var max_height = 0;

                    $(this).children().children('.nav-column').each(function(){
                        if(max_height < $(this).height()){
                            max_height = $(this).height();
                        }
                    });
                    $(this).children().children('.nav-column').each(function(){
                        $(this).height(max_height);
                    });

                });
            }

        }

        $mrMegamenu.setPosition = function(){
            $(this).children('li').each(function(){
                var position = $(this).position();
                var fullWidth = $(this).parent().width();
                var left = position.left;
                var width = $(this).width();
                var childWidth = $(this).children('div').width();

                if(childWidth < fullWidth - left){
                    $(this).children('div').css('left', left);
                }
                else{
                    if(childWidth > Math.max(fullWidth - left, left + width)){
                        if(left <= fullWidth/2){
                            $(this).children('div').css('left', 0);
                        }
                        // else{
                        //     $(this).children('div').css('left', fullWidth - childWidth);
                        // }
                    }
                    else{
                        // $(this).children('div').css('left', fullWidth - childWidth);
                    }
                }

            });
        }

        return $mrMegamenu.each(function($settings){
            $mrMegamenu.initRowItems();
            $mrMegamenu.setPosition();
        });

    }
})(jQuery);